package com.stockmarket.stock.controllers;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.stock.dtos.BaseResponse;
import com.stockmarket.stock.dtos.StockDTO;
import com.stockmarket.stock.dtos.StockDetailsResponseDTO;
import com.stockmarket.stock.services.StockService;

@RestController
@RequestMapping("/api/v1.0/market/stock")
@CrossOrigin
public class StockController {

	@Autowired
	private StockService stockService;

	@PostMapping(value = "/add/{companyCode}")
	BaseResponse<StockDTO> addStockPrice(@PathVariable String companyCode, @RequestBody StockDTO stockDTO) {
		BaseResponse<StockDTO> respone = stockService.addStockPrice(companyCode, stockDTO);
		return respone;
	}

	@GetMapping(value = "/get/{companyCode}/{startDate}/{endDate}")
	BaseResponse<StockDetailsResponseDTO> getStockPrices(@PathVariable String companyCode,
			@PathVariable(name = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
			@PathVariable(name = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate) {
		BaseResponse<StockDetailsResponseDTO> respone = stockService.getStockPrices(companyCode, startDate, endDate);
		return respone;
	}

	@GetMapping(value = "/getLatest/{companyCode}")
	BaseResponse<StockDTO> getLatestStockPrice(@PathVariable String companyCode) {
		BaseResponse<StockDTO> respone = stockService.getLatestStockPriceByCompanyCode(companyCode);
		return respone;
	}

	@DeleteMapping(value = "/delete/{companyCode}")
	BaseResponse<?> deleteStocks(@PathVariable String companyCode) {
		BaseResponse<?> respone = stockService.deleteStocksByCompanyCode(companyCode);
		return respone;
	}
	
	@GetMapping(value = "/stockexchanges")
	BaseResponse<List<String>> getAllStockExchanges() {
		BaseResponse<List<String>> respone = stockService.getAllStockExchanges();
		return respone;
	}

}
