package com.stockmarket.stock.repositories;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.stockmarket.stock.models.Stock;

@Repository
public interface StockRepository extends CassandraRepository<Stock, UUID> {

	@AllowFiltering
	List<Stock> findByCompanyCodeAndIsDeletedFalse(String companyCode);

	@AllowFiltering
	Stock findTopByCompanyCodeAndIsDeletedFalse(String companyCode);


	@AllowFiltering
	List<Stock> findByCompanyCodeAndDateTimeBetweenAndIsDeletedFalse(String companyCode, Timestamp startDate,
			Timestamp endDate);

	@AllowFiltering
	Stock findTopByCompanyCodeAndIsDeletedFalseOrderByDateTime(String companyCode);
}
