package com.stockmarket.stock.services;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.uuid.Generators;
import com.stockmarket.stock.dtos.BaseResponse;
import com.stockmarket.stock.dtos.StockDTO;
import com.stockmarket.stock.dtos.StockDetailsResponseDTO;
import com.stockmarket.stock.dtos.StockExchange;
import com.stockmarket.stock.models.Stock;
import com.stockmarket.stock.repositories.StockRepository;

@Service
public class StockService {

	@Autowired
	StockRepository stockRepository;

	public BaseResponse<StockDTO> addStockPrice(String companyCode, StockDTO stockDTO) {
		BaseResponse<StockDTO> baseResponse = new BaseResponse<>();
		try {
			if (StringUtils.isEmpty(companyCode)) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company code should be manadatory");
				return baseResponse;
			}
			if (stockDTO.getStockPrice() == null) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Please enter a fractional value for stock price");
				return baseResponse;
			}

			Stock stock = new Stock();
			stock.setId(Generators.timeBasedGenerator().generate());
			;
			stock.setCompanyCode(companyCode);
			stock.setStockPrice(stockDTO.getStockPrice());
			stock.setStockExchange(stockDTO.getStockExchange());
			stock.setDateTime(LocalDateTime.now());
			stock.setIsDeleted(false);
			stockRepository.save(stock);

			BeanUtils.copyProperties(stock, stockDTO);
			baseResponse.setData(stockDTO);
			baseResponse.setMessage("Stock price added for the company code: " + companyCode);
			baseResponse.setStatus("success");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<StockDetailsResponseDTO> getStockPrices(String companyCode, Date startDate, Date endDate) {
		BaseResponse<StockDetailsResponseDTO> baseResponse = new BaseResponse<>();
		try {
			StockDetailsResponseDTO stockDetailsResponseDTO = new StockDetailsResponseDTO();
			List<StockDTO> stockDTOs = new ArrayList<StockDTO>();

			Date startDateToFormat = startDate;
			LocalDateTime startDateTimeToFFormat = LocalDateTime.ofInstant(startDateToFormat.toInstant(),
					ZoneId.systemDefault());
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			String startDateTime = startDateTimeToFFormat.format(formatter);

			Date endDateToFormat = endDate;
			LocalDateTime endDateTimeToFFormat = LocalDateTime.ofInstant(endDateToFormat.toInstant(),
					ZoneId.systemDefault());
			String endDateTime = endDateTimeToFFormat.format(formatter);

			List<Stock> stocks = stockRepository.findByCompanyCodeAndDateTimeBetweenAndIsDeletedFalse(companyCode,
					Timestamp.valueOf(startDateTime), Timestamp.valueOf(endDateTime));

			List<Double> stockPrices = new ArrayList<Double>();
			stocks.forEach(stock -> {
				StockDTO stockDTO = new StockDTO();

				stockDTO.setCompanyCode(stock.getCompanyCode());
				stockDTO.setStockExchange(stock.getStockExchange());
				stockDTO.setStockPrice(stock.getStockPrice());
				stockDTO.setDateTime(stock.getDateTime());

				stockPrices.add(stock.getStockPrice());
				stockDTOs.add(stockDTO);

			});

			Double maxPrice = stockPrices.stream().max(Comparator.naturalOrder()).get();
			Double minPrice = stockPrices.stream().min(Comparator.naturalOrder()).get();
			OptionalDouble avgPriceOpt = stockPrices.stream().mapToDouble(m -> m.doubleValue()).average();
			Double avgPrice = avgPriceOpt.getAsDouble();

			stockDetailsResponseDTO.setStockDTOs(stockDTOs);
			stockDetailsResponseDTO.setMaxStockPrice(maxPrice);
			stockDetailsResponseDTO.setMinStockPrice(minPrice);
			stockDetailsResponseDTO.setAvgStockPrice(avgPrice);

			baseResponse.setStatus("success");
			baseResponse.setMessage("Fetching stock details for the company code: " + companyCode);
			baseResponse.setData(stockDetailsResponseDTO);
			;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<?> deleteStocksByCompanyCode(String companyCode) {
		BaseResponse<?> baseResponse = new BaseResponse<>();

		try {

			List<Stock> stocks = stockRepository.findByCompanyCodeAndIsDeletedFalse(companyCode);
			if (CollectionUtils.isEmpty(stocks)) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("No stocks found for the company code: " + companyCode);
				return baseResponse;
			}
			stocks.forEach(stock -> {
				stock.setIsDeleted(true);
				stockRepository.save(stock);
			});

			baseResponse.setStatus("success");
			baseResponse.setMessage("Stocks deleted succesfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<StockDTO> getLatestStockPriceByCompanyCode(String companyCode) {
		BaseResponse<StockDTO> baseResponse = new BaseResponse<>();

		try {

			List<Stock> stocks = stockRepository.findByCompanyCodeAndIsDeletedFalse(companyCode);
			if(CollectionUtils.isEmpty(stocks)) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("No stocks found for the company code: " + companyCode);
				return baseResponse;
			}
			
			List<Stock> stocksSortedByDate = stocks.stream()
					  .sorted(Comparator.comparing(Stock::getDateTime).reversed())
					  .collect(Collectors.toList());
			Stock latestStock = stocksSortedByDate.get(0);
			
			if(latestStock == null) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("No latest stocks found after sorting.");
				return baseResponse;
			}

			StockDTO stockDTO = new StockDTO();
			stockDTO.setCompanyCode(companyCode);
			stockDTO.setStockExchange(latestStock.getStockExchange());
			stockDTO.setStockPrice(latestStock.getStockPrice());
			stockDTO.setDateTime(latestStock.getDateTime());

			baseResponse.setStatus("success");
			baseResponse.setMessage("Fetching latest stock details..");
			baseResponse.setData(stockDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<List<String>> getAllStockExchanges() {	
		BaseResponse<List<String>> baseResponse = new BaseResponse<List<String>>();
		try {
			
			List<String> stockExchanges = new ArrayList<String>();
			
			stockExchanges.add(StockExchange.BSE.name());
			stockExchanges.add(StockExchange.NSE.name());
			
			baseResponse.setStatus("success");
			baseResponse.setMessage("Fetching stock exchanges..");
			baseResponse.setData(stockExchanges);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
		
	}

}
