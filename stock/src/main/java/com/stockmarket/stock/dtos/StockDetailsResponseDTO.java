package com.stockmarket.stock.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockDetailsResponseDTO {

	List<StockDTO> stockDTOs;
	
	private Double maxStockPrice;
	
	private Double minStockPrice;

	private Double avgStockPrice;	

}
