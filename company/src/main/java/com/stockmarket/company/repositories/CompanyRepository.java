package com.stockmarket.company.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stockmarket.company.models.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	Company findByCodeAndIsDeletedFalse(String companyCode);

	List<Company> findAllByIsDeletedFalse();

}
