package com.stockmarket.company.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmarket.company.dtos.BaseResponse;
import com.stockmarket.company.dtos.CompanyDTO;
import com.stockmarket.company.dtos.CompanyDetailsResponseDTO;
import com.stockmarket.company.services.CompanyService;


@RestController
@RequestMapping("/api/v1.0/market/company")
@CrossOrigin
public class CompanyController {
	
	@Autowired
	CompanyService companyService;
	
	@PostMapping(value = "/register")
	BaseResponse<CompanyDTO> registerCompany(@RequestBody CompanyDTO companyDTO) {
		BaseResponse<CompanyDTO> respone = companyService.registerCompany(companyDTO); 
		return respone;
	}
	
	@GetMapping(value = "/info/{companyCode}")
	BaseResponse<CompanyDetailsResponseDTO> getCompanyDetails(@PathVariable String companyCode) {
		BaseResponse<CompanyDetailsResponseDTO> respone = companyService.getCompanyDetailsByCompanyCode(companyCode); 
		return respone;
	}
	
	@GetMapping(value = "/getall")
	BaseResponse<List<CompanyDetailsResponseDTO>> getAllCompanyDetails() {
		BaseResponse<List<CompanyDetailsResponseDTO>> respone = companyService.getAllCompanyDetails(); 
		return respone;
	}
	
	@DeleteMapping(value = "/delete/{companyCode}")
	BaseResponse<?> deleteCompanyDetails(@PathVariable String companyCode) {
		BaseResponse<?> respone = companyService.deleteCompanyDetailsByCompanyCode(companyCode); 
		return respone;
	}
	
	

}
