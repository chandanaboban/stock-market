package com.stockmarket.company.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.stockmarket.company.dtos.BaseResponse;
import com.stockmarket.company.dtos.CompanyDTO;
import com.stockmarket.company.dtos.CompanyDetailsResponseDTO;
import com.stockmarket.company.dtos.StockDTO;
import com.stockmarket.company.models.Company;
import com.stockmarket.company.repositories.CompanyRepository;

@Service
public class CompanyService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	CompanyRepository companyRepository;

	public BaseResponse<CompanyDTO> registerCompany(CompanyDTO companyDTO) {
		BaseResponse<CompanyDTO> baseResponse = new BaseResponse<>();
		try {

			if (StringUtils.isEmpty(companyDTO.getName())) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company name cannot be empty or null");
				return baseResponse;
			}

			if (StringUtils.isEmpty(companyDTO.getCode())) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company code cannot be empty or null");
				return baseResponse;
			}

			if (StringUtils.isEmpty(companyDTO.getCeo())) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company CEO cannot be empty or null");
				return baseResponse;
			}

			if (StringUtils.isEmpty(companyDTO.getWebsite())) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company website cannot be empty or null");
				return baseResponse;
			}

			if (companyDTO.getTurnover() == null) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company turnover cannot be empty or null");
				return baseResponse;
			}

			if (companyDTO.getTurnover() < 100000000) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company turnover should be greater than 10 crores");
				return baseResponse;
			}

			Company company = new Company();
			BeanUtils.copyProperties(companyDTO, company);
			company.setIsDeleted(false);
			Company companySaved = companyRepository.save(company);

			BeanUtils.copyProperties(companySaved, companyDTO);
			baseResponse.setData(companyDTO);
			baseResponse.setMessage("Company has been registered succesfully");
			baseResponse.setStatus("success");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<CompanyDetailsResponseDTO> getCompanyDetailsByCompanyCode(String companyCode) {
		BaseResponse<CompanyDetailsResponseDTO> baseResponse = new BaseResponse<>();
		try {
			Company companyByCode = companyRepository.findByCodeAndIsDeletedFalse(companyCode);
			if (companyByCode == null) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company does not exist for the company code: " + companyCode);
				return baseResponse;
			}

			// ---Fetching latest stock price from stock microservice

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			ResponseEntity<BaseResponse<StockDTO>> response = restTemplate.exchange(
					"http://localhost:8081/api/v1.0/market/stock/getLatest/" + companyCode, HttpMethod.GET, null,
					new ParameterizedTypeReference<BaseResponse<StockDTO>>() {
					});

			if (response.getBody() == null) {
				baseResponse.setMessage("No stock details found for the company code: " + companyCode);
				baseResponse.setStatus("success");
				return baseResponse;
			}
			BaseResponse<StockDTO> baseResponseRestTemplate = response.getBody();

			if (baseResponseRestTemplate.getData() == null) {
				baseResponse.setMessage("No stock data found for the company code: " + companyCode);
				baseResponse.setStatus("success");
				return baseResponse;
			}

			StockDTO stockDTOResponse = baseResponseRestTemplate.getData();

			if (stockDTOResponse == null) {
				baseResponse.setMessage("Stock response is null for the company code: " + companyCode);
				baseResponse.setStatus("success");
				return baseResponse;
			}

			CompanyDetailsResponseDTO companyDetailsDTO = new CompanyDetailsResponseDTO();
			BeanUtils.copyProperties(companyByCode, companyDetailsDTO);
			companyDetailsDTO.setLatestStockPrice(stockDTOResponse.getStockPrice());
			companyDetailsDTO.setStockExchange(stockDTOResponse.getStockExchange());
			baseResponse.setData(companyDetailsDTO);
			baseResponse.setMessage("Fetching company details..");
			baseResponse.setStatus("success");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<List<CompanyDetailsResponseDTO>> getAllCompanyDetails() {
		BaseResponse<List<CompanyDetailsResponseDTO>> baseResponse = new BaseResponse<>();
		try {
			List<CompanyDetailsResponseDTO> companyDetailsList = new ArrayList<>();

			List<Company> companies = companyRepository.findAllByIsDeletedFalse();

			for (Company company : companies) {

				CompanyDetailsResponseDTO companyDetailsResponseDTO = new CompanyDetailsResponseDTO();
				BeanUtils.copyProperties(company, companyDetailsResponseDTO);
				// ---fetching latest stock details from stock microservice
				HttpHeaders requestHeaders = new HttpHeaders();
				requestHeaders.setContentType(MediaType.APPLICATION_JSON);
				requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

				ResponseEntity<BaseResponse<StockDTO>> response = restTemplate.exchange(
						"http://localhost:8081/api/v1.0/market/stock/getLatest/" + company.getCode(), HttpMethod.GET,
						null, new ParameterizedTypeReference<BaseResponse<StockDTO>>() {
						});

				if (response.getBody() != null) {
					BaseResponse<StockDTO> baseResponseRestTemplate = response.getBody();
					if (baseResponseRestTemplate.getData() != null) {
						StockDTO stockDTOResponse = baseResponseRestTemplate.getData();
						if (stockDTOResponse != null) {

							companyDetailsResponseDTO.setLatestStockPrice(stockDTOResponse.getStockPrice());
							companyDetailsResponseDTO.setStockExchange(stockDTOResponse.getStockExchange());

						}
					}
				}

				companyDetailsList.add(companyDetailsResponseDTO);

			}
			baseResponse.setData(companyDetailsList);
			baseResponse.setMessage("Fetching all company details..");
			baseResponse.setStatus("success");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

	public BaseResponse<?> deleteCompanyDetailsByCompanyCode(String companyCode) {
		BaseResponse<?> baseResponse = new BaseResponse<>();
		try {
			Company companyByCode = companyRepository.findByCodeAndIsDeletedFalse(companyCode);
			if (companyByCode == null) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Company name does not exists for the company code: " + companyCode);
				return baseResponse;
			}
			companyByCode.setIsDeleted(true);
			companyRepository.save(companyByCode);
			// ---Calling deleteStock api of Stock microservice

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			ResponseEntity<BaseResponse<StockDTO>> response = restTemplate.exchange(
					"http://localhost:8081/api/v1.0/market/stock/delete/" + companyCode, HttpMethod.DELETE, null,
					new ParameterizedTypeReference<BaseResponse<StockDTO>>() {
					});
			BaseResponse<StockDTO> baseResponseRestTemplate = response.getBody();

			if (StringUtils.isEmpty(baseResponseRestTemplate.getMessage())) {
				baseResponse.setStatus("success");
				baseResponse.setMessage("Stock microservice response is empty");
				return baseResponse;
			}

			baseResponse.setStatus("success");
			baseResponse.setMessage("Company deleted succesfully & " + baseResponseRestTemplate.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseResponse;
	}

}
