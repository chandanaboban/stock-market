package com.stockmarket.company.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDetailsResponseDTO {
	
	private String name;
	
	private String code;
	
	private String ceo;
	
	private Long turnover;
	
	private String website;
	
	private Double latestStockPrice;
	
	private String stockExchange;
	
	private Boolean isDeleted;

}
