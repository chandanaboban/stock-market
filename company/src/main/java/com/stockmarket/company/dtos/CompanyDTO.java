package com.stockmarket.company.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDTO extends BaseResponse {
	
	private String name;
	
	private String code;
	
	private String ceo;
	
	private Long turnover;
	
	private String website;
	
	private Boolean isDeleted;

}
